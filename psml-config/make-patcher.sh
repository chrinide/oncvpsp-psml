#!/bin/sh
#
# 
PATCH_PREFIX=$(basename $(bzr root))
PATCHED_SOURCE_DIRNAME=${PATCH_PREFIX}
#
ONCVPSP_PKG=oncvpsp-3.3.1.tar.gz
PKG_MD5=e21020c08d2f8f24db6fddce08ba585e
#
ONCVPSP_DIRNAME=oncvpsp-3.3.1
PATCH_LEVEL=${PATCH_PREFIX}-$(bzr revno)
PATCH_FILE=${ONCVPSP_DIRNAME}--${PATCH_LEVEL}.patch
#
PKG_NAME=patcher--${ONCVPSP_DIRNAME}--${PATCH_LEVEL}

# Go up two levels to prepare a patch without ".."
#
cd ../..
rm -rf ${PKG_NAME} ;  mkdir ${PKG_NAME}
#
# We assume that the source directory is checked out as 'psml-patch'
# This might change
# We pipe the patch through sed to update the "creator" string in m_psmlout.f90
#
diff -Naur -x .bzr -x psml-config  \
     ${ONCVPSP_DIRNAME}     \
     ${PATCHED_SOURCE_DIRNAME} |   \
    LC_ALL=C sed "s/__PSML_PATCH__/${PATCH_LEVEL}/g"  > \
	              ${PKG_NAME}/${PATCH_FILE}
#
# Go back
#
cd ${PATCHED_SOURCE_DIRNAME}/psml-config
cp -p README get_xmlf90.sh ../../${PKG_NAME}
LC_ALL=C sed "s/__PSML_PATCH__/${PATCH_LEVEL}/g" setup.sh.in > ../../${PKG_NAME}/setup.sh
#
# Generate tarball
#
cd ../..
tar czf ${PKG_NAME}.tgz ${PKG_NAME}


#!/bin/bash
#runs an example and compares with reference out file

# First argument: runner (eg: run.sh, run_r.sh, run_nr.sh)
# Second argument: case name (e.g. 07_N)
#

runner=$1
case=$2

../$runner $case -np
../compare.sh $case

grep Summary $case.diff | sed -e /diff/s/^/\\\n/ 

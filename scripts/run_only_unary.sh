#!/bin/bash
# runs an example, without comparing to the reference
# (this script will be used if perl is not installed)

# First argument: runner (eg: run.sh, run_r.sh, run_nr.sh)
# Second argument: case name (e.g. 07_N)
#

runner=$1
case=$2

../$runner $case -np

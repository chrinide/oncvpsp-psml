This is version 3.3.1 of the ONCVPSP package by Don Hamann, supplemented
by the ability to generate pseudopotential files in PSML format.

The original code can be downloaded from http://www.mat-simresearch.com/, and
is also available in the 'upstream' branch in this repo.

The original documentation is in the doc/ subdirectory.

A list of the (minor) changes introduced to implement the PSML output functionality
is in 00_README_PSML_VERSION.md.

To install, see INSTALL and INSTALL.CMake.md.

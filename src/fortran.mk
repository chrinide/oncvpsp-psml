#
# 
#
F77        = gfortran	#EDIT
F90        = gfortran	#EDIT
CC         = gcc	#EDIT
RANLIB     = echo
AR         = ar

FLINKER     = $(F90)

FFLAGS     = -O3 -msse3 #EDIT
CFLAGS     = -O3		#EDIT

FCCPPFLAGS = -DLIBXC_VERSION=203  #This probably should not be changed

#LIBS = -L/usr/local/opt/lapack/lib -llapack  -L/usr/local/opt/openblas/lib  -lopenblas	#EDIT

#
# This line works for MacOSX with veclibFort (a patch for Apple's veclib)
# (See https://github.com/mcg1969/vecLibFort)
# You might get good results simply with -framework veclib, but your mileage might vary
# In case of doubt, get any of the fine blas/lapack packages out there.
#
LIBS = -lvecLibFort

#
# The xmlf90 library (its wxml subsystem) is needed to generate XML.
# You can download xmlf90 from http://launchpad.net/xmlf90
#
XMLF90_ROOT=$(HOME)/lib/gfortran-5.2.0/xmlf90-1.5.0
LIBS += -L$(XMLF90_ROOT)/lib -lxmlf90
INC += -I$(XMLF90_ROOT)/include

#
# Optional LIBXC support

# oncvpsp is presently compatible with libxc-2.0.3, not later releases (???)

# To build oncvpsp with libxc, use the following lines and edit
# the paths to point to your libxc library and include directories
# make clean in src before rebuilding after changing this
#
LIBXC_ROOT=$(HOME)/lib/libxc/2.2.0/gfortran-5.2.0
LIBS += -L$(LIBXC_ROOT)/lib -lxc -lxcf90
INC += -I$(LIBXC_ROOT)/include
OBJS_LIBXC =	functionals.o exc_libxc.o
#
# Otherwise, use only the following line
#
#OBJS_LIBXC =	exc_libxc_stub.o
#
#-------------------------------------------


#

#
.f90.o:
	$(F90) $(FFLAGS) $(INC) -c $*.f90
.F90.o:
	$(F90) $(FFLAGS) $(FCCPPFLAGS) $(INC) -c $*.F90
.f.o:
	$(F90) $(FFLAGS) $(INC) -c $*.f
.F.o:
	$(F90) $(FFLAGS) $(FCCPPFLAGS) $(INC) -c $*.F


This version of oncvpsp contains extra funcionality to generate PSML files
(See doc/PSML_output.txt)

Changes to the original code are minimal:

* A CMake building framework (that transparently coexists with the old one).

* An extra module contained in m_psmlout.F90 implements the needed
  output routines.
* m_getopts.f90 and m_uuid.f90 implement lower-level functionality.

* modcore* routines have an extra argument to pass back information
  about the pseudo-core radius.
* oncvpsp* driver programs have an extra PSML id in the version string,
  and calls to the new PSML-related routines.

* The scripts in 'tests' (copies of those in 'scripts' with an updated
  path) have been removed from the distribution are now explicitly
  created by the 'set_path' utility.

* A new 'run_and_compare_unary.sh' script to facilitate test organization.

* The make.inc file has been renamed to make.inc.sample, and a new directory
  with sample files (sys_make_incs) created.

* The run*sh scripts have been modified to generate a <prefix>.psml
  file automatically. 

* The symbol name in the 57_La test has been corrected in both the .dat file
  and the reference output file. (It was 'la' instead of 'La', which caused
  problems for the PSML subsystem)


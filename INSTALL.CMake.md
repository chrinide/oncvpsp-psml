# Installing oncvpsp-psml with CMake

   Try first the fully automatic method:

```
   cmake -S. -B_build
   cmake --build _build
   cmake --build _build --target test
   cmake --install _build --prefix /path/to/optional/installation
```

* In the above example the compiler flags are the default ones. To use custom flags, use
  the FFLAGS environment variable:

```
  FFLAGS="-O0 -fcheck=all -g" cmake -S. -B_build
```

* The comparison of test outputs with reference values is contingent
  on the presence of a perl interpreter in the system.  Comparisons can be
  disabled altogether by setting

```
  -DENABLE_TEST_OUTPUT_CHECK=OFF
```

## LAPACK options

   If CMake was not able to find a LAPACK installation, you can help it by
   defining

```
   -DBLAS_LIBRARIES=/path/to/appropriate/place
```

   or

```
   -DBLAS_LIBRARIES=libname
```

   For example, on a Mac with veclibfort (an essential [fortran
   wrapper](https://github.com/mcg1969/vecLibFort) for the built-in
   optimized 'Accelerate' framework), one could use a line similar
   to any of these:

```
   -DBLAS_LIBRARIES=veclibfort
   -DBLAS_LIBRARIES="-L/opt/homebrew/lib -lveclibfort"
```

   (This is encoded in the 'toolchain' file config/cmake/toolchains/mac.cmake. Other
    toolchain files can be prepared appropriately. Then for example:

```
      cmake -S. -B_build -C config/cmake/toolchains/mac.cmake
```

   In some systems you might have to set in an analogous manner:

```
   -DLAPACK_LIBRARIES=.....
```

   LAPACK search is done with the standard CMake FindLAPACK
   module. See the documentation
   [here](https://cmake.org/cmake/help/latest/module/FindLAPACK.html#findlapack)
   for all the options.  If you have OpenBlas, for example, you could
   do

```
      cmake -S. -B_build -DBLA_VENDOR=Openblas
```

## Libxc support

   If you want libxc support, define WITH_LIBXC=ON and, if needed, tell
   CMake where to find the libxc library, either with CMAKE_PREFIX_PATH:


```
    cmake -S. -B_build -DWITH_LIBXC=ON -DCMAKE_PREFIX_PATH=/path/to/libxc
```

   or by defining the environment variable LIBXC_ROOT to point to the installation:


```
    LIBXC_ROOT=/path/to/libxc cmake -S. -B_build -DWITH_LIBXC=ON 
```

   This version of oncvpsp can use libxc versions 2.X up to 4.X. CMake knows
   about this and will stop if this is not satisfied.
   


